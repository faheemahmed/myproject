import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
{ 
  path:'auth',loadChildren:() => import('./authorization/authorization.module').then(a => a.AuthorizationModule)
},
{ 
  path :'admin',loadChildren:() => import('./admin/admin.module').then(n => n.AdminModule) 
},
{
  path:'buyer',loadChildren:() => import('./buyer/buyer.module').then(b => b.BuyerModule)
},
{
  path:'seller',loadChildren:()=> import('./seller/seller.module').then(s => s.SellerModule)
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
