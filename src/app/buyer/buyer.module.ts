import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerComponent } from './buyer.component';



@NgModule({
  declarations: [BuyerComponent],
  imports: [
    CommonModule
  ]
})
export class BuyerModule { }
