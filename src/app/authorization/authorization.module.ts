import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorizationRoutingModule } from './authorization-routing.module';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthorizationComponent } from './authorization.component';
import {InputTextModule} from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [LoginComponent, SignUpComponent, AuthorizationComponent],
  imports: [
    CommonModule,
    AuthorizationRoutingModule,
    InputTextModule,
    FormsModule,
  ]
})
export class AuthorizationModule { }
